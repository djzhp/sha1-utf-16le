import hashlib
import itertools
from concurrent.futures import ThreadPoolExecutor
from tqdm import tqdm

def sha1_hash(input_string):
    hasher = hashlib.sha1()
    hasher.update(input_string.encode('utf-16le'))
    return "0x" + hasher.hexdigest().upper()

def check_password(password, target_hash):
    if sha1_hash(password) == target_hash:
        return password

def generate_passwords(characters, fixed_length):
    """生成指定长度的所有密码组合的生成器"""
    return (''.join(combo) for combo in itertools.product(characters, repeat=fixed_length))

def main():
    target_hash = input("请输入目标哈希值：")
    characters = '0123456789'  # 只包含数字
    fixed_length = 6  # 固定长度为6位数字

    passwords = generate_passwords(characters, fixed_length)
    total_passwords = 10 ** fixed_length  # 计算总密码数（10的6次方）

    with ThreadPoolExecutor() as executor:
        # 使用tqdm创建一个进度条
        with tqdm(total=total_passwords, unit="pwd", desc="破解进度") as progress:
            results = executor.map(lambda p: check_password(p, target_hash), passwords)
            for result in results:
                progress.update(1)  # 每检查一个密码，进度条更新一次
                if result:
                    print(f"破解成功，密码是：{result}")
                    return

    print("破解失败，未找到匹配的密码。")

if __name__ == "__main__":
    main()
