import hashlib


def sha1_encode(plaintext):
    hasher = hashlib.sha1()
    hasher.update(plaintext.encode('utf-16le'))
    return "0x" + hasher.hexdigest().upper()

def main():
    plaintext = input("请输入明文：")
    encrypted = sha1_encode(plaintext)
    print("生成的密文：", encrypted)

if __name__ == "__main__":
    main()
